#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any search will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Stucture to hold both a plaintext search and a hash.
struct entry 
{
    char pass[PASS_LEN];
    char hash[HASH_LEN];
};

int byName(const void *a, const void *b)
{
    struct entry *aa = (struct entry *)a;
    struct entry *bb = (struct entry *)b;
    
    return strcmp(aa->hash, bb->hash);
}

int searchByName(const void *t, const void *elem)
{
	char *tt = (char *)t;
    struct entry *eelem = (struct entry*)elem;
    
	return strcmp(tt, eelem->hash);
}


// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    // Open the dictionary file 
    FILE *dictFile = fopen(filename, "r");
	if (!dictFile)
	{
	    perror("Can't open dictionary file.");
	    exit(1);
	}
	
	int arrayLength = 100;
	int entries = 0;
	struct entry *array = malloc(arrayLength * sizeof(struct entry));
	
	// Loop through the dictionary file, one password
    // at a time while reading each one of them
	char password[PASS_LEN];
    while(fgets(password, PASS_LEN, dictFile) != NULL)
    {
        char *nl = strchr(password, '\n');
        if (nl != NULL)
        {
     		*nl = '\0';
     	}
        
        // Array is full. Make it bigger by 25%
        if (entries == arrayLength)
        {
            arrayLength += (arrayLength * .25);
        	array = realloc(array, arrayLength * sizeof(struct entry));
        }
        
        // Hashing each password.
        char *hash = md5(password, strlen(password));
        char *str = malloc(20 * sizeof(char));
        strcpy(str,password);
        
        // Copy hashed to the entry
        strcpy(array[entries].pass, str);
        strcpy(array[entries].hash, hash);
        
        entries++;
    }
	
    *size = entries;
    
    // Close the dictionary file
    fclose(dictFile);
    
	// Return pointer to the array
    return array;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hashFile dictFile\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of entry structures
    int dictSize;
    struct entry *dict = read_dictionary(argv[2], &dictSize);
    
    // sorts the array by hash value.
    qsort(dict, dictSize, sizeof(struct entry), byName);

    // Open the hash file for reading.
    FILE *hashFile = fopen(argv[1], "r");
    if (!hashFile)
    {
        fprintf(stderr, "Can't open %s for reading.\n", argv[1]);
        exit(1);
    }

    // Loop through the hash file, one password
    // at a time searching for each hash using binary search.
    char hash[HASH_LEN];
    while(fgets(hash, HASH_LEN, hashFile) != NULL)
    {
        // Trim the new password char
        char *nl = strchr(hash, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
        
        // search for the hash and once finding it, get the corresponding plaintext dictionary word.
        // Print out both the hash and word.
        struct entry *found = bsearch(hash, dict, dictSize, sizeof(struct entry), searchByName);
        if(found != NULL)
        {
        	printf("Found it!\n");
        	printf("%s %s\n", found->pass, found->hash);;
        }
    }
    
    // Close the hash file
    fclose(hashFile);
    return 0;
}
